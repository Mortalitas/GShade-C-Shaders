**Permission has been granted to the GShade project for distribution rights to several shaders and textures by the authors, whose licenses or copyrights otherwise do not allow for redistribution, rehosting, or automated downloading. Please contact the creators directly for permission if you are interested in doing so:**

* Espresso Lalafell (twitter.com/espressolala)
  * EGGameplayLut.png
  * FFXIVLUTAtlas.png

* Fairy (instagram.com/fairyqueen.xiv)
  * Fairy PROJECT.jpg
  * FairyAbandoned.png
  * FairyArdenvaldDark.png
  * FairyArdenvaldGlow.png
  * FairyArdenvaldLight.png
  * FairyBitchesBrew.png
  * FairyBlackFog.png
  * FairyBlackSwan.jpg
  * FairyBloodyMary.png
  * FairyBluefire.png
  * FairyBrokenIce.png
  * FairyBrokenIceLight.png
  * FairyButterflyheart.png
  * FairyCelestialNights.png
  * FairyCelestialNightsLayer.png
  * FairyColdOutside.jpg
  * FairyCrystallized.jpg
  * FairyDandelion.png
  * FairyDrivingHomeForChristmas.png
  * FairyFinsOceanHorror.png
  * FairyFinsOceanHorror02.png
  * FairyHeartOnAString.png
  * FairyHearts.jpg
  * FairyHearty.png
  * FairyHikkidasTimewarp.jpg
  * FairyKawaii.png
  * FairyLunasGarden.png
  * FairyMelonsplash.png
  * FairyMermaid.png
  * FairyMermen.png
  * FairyMerryWhatever.jpg
  * FairyNarcoticFog.png
  * FairyPersephone01.png
  * FairyPersephone02.png
  * FairyPolaroid.png
  * FairyPolaroidDark.png
  * FairyPrism.png
  * FairyPurgatory.jpg
  * FairyRain.png
  * FairyRavenqueenFog.png
  * FairyREC.png
  * FairyREC2.png
  * FairyShiroxoNeonLights.jpg
  * FairySnowtastic.png
  * FairyThisIsHalloween.png
  * FairyValanthyne.png
  * FairyVampirequeen_Bats.jpg
  * FairyVampirequeenFog.jpg
  * FairyVampiresDance.jpg
  * FairyWhiteSwan.jpg
  * FairyWintergreen.jpg
  * FairyWitchesBrew.png
  * FairyYaleerasEden.png

* Feli (twitter.com/ffxivfeli)
  * lut_Feli.png

* Johnni Maestro (twitter.com/Johnnicles)
  * PopArt.png
  * Vaporwave.jpg
  * VignetteSharp.png
  * VignetteSoft.png

* Johto (twitter.com/JohtoXIV)
  * MultiLut_Johto.png

* LordCobra (github.com/LordKobra):
  * Cobra_Mask.fx
  * ColorSort.fx
  * computeGravity.fx
  * Droste.fx
  * LongExposure.fx
  * RealLongExposure.fx

* Lufreine (twitter.com/Lufreine)
  * Ice1.jpg
  * Metal1.jpg
  * Papyrus2.png
  * Papyrus6.png

* Marot (twitter.com/MarotSatil)
  * MultiLut_Marot.png

* MartyMcFly/Pascal Glitcher (github.com/martymcmodding):
  * AreaLUT.png
  * ca_lut_new.png
  * iMMERSE_bluenoise.png
  * MartysMods_CHROMATICABERRATION.fx
  * MartysMods_FILMGRAIN.fx
  * MartysMods_HALFTONE.fx
  * MartysMods_LAUNCHPAD.fx
  * MartysMods_LOCALLAPLACIAN.fx
  * MartysMods_LONGEXPOSURE.fx
  * MartysMods_MXAO.fx
  * MartysMods_NVSHARPEN.fx
  * MartysMods_SHARPEN.fx
  * MartysMods_SMAA.fx
  * MartysMods_TODDYHANCER.fx
  * Posterize.fx
  * qUINT_bloom.fx
  * qUINT_common.fxh
  * qUINT_deband.fx
  * qUINT_dof.fx
  * qUINT_lightroom.fx
  * qUINT_mxao.fx
  * qUINT_sharp.fx
  * qUINT_ssr.fx
  * Retro_Neon.fx

* Mori (twitter.com/moripudding)
  * lut_GShade.png
  * LayerA.png
  * MultiLut_GShade.png

* Neneko (twitter.com/Xelyanne)
  * MultiLut_Neneko.png

* Nightingale Silence (twitter.com/franandneo)
  * MultiLut_Nightingale.png

* ninjafada (sfx.thelazy.net/users/u/ninjafada)
  * lut_ninjafadaGameplay.png
  * MultiLut_ninjafadaGameplay.png

* Talim (twitter.com/Talim_ffxiv)
  * TalimAladdin.png
  * TalimAladdin2.png
  * TalimBlueLight.png
  * TalimBubblebun.png
  * TalimColorBorder.png
  * TalimGalaxy.png
  * TalimGreen.png
  * TalimNightSky.png
  * TalimRed.png
  * TalimSmoke.png
  * TalimSpiderweb.png

* TreyM (github.com/IAmTreyM)
  * ATMOSPHERE.fx
  * CustomNegative.png
  * CustomPrint.png
  * FILMDECK.fx
  * Grain.png
  * FSR1_2X.fx
  * ross.png
  * StandardNegativeAtlas.png
  * StandardPrintAtlas.png

* Yae (twitter.com/lapismenangis)
  * MultiLut_yaes.png

* Yomigami Okami (twitter.com/Yomigammy)
  * Fire1.png
  * Fire2.png
  * Lightrays1.png
  * Shatter1.png
  * Snow1.png
  * Snow2.png

**For rights regarding all other shaders and textures, please refer to the licenses inside each shader file.**